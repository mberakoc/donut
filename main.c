#include "lib/donut.h"

int main() {
    String string = _String("");
    printf("%s\n", is_empty(string) == true ? "Empty" : "Not empty");
    string = _StringWithAnother(join((String[]){_String("Hello"),
                                                _String("World!")}, 2,
                                                        _String(" ")));
    print_string(string);
    printf("Does \"%s\" contain \"Hell\"? %s\n", convert_to_char_array(string),
           contains(string, _String("Hell")) ? "Yes" : "No");
    replace(string, _String("World"), _String("Saturn"));
    print_string(string);
    string = _String("  Trim me now!     ");
    trim(&string);
    print_string(string);
    return EXIT_SUCCESS;
}
