#ifndef DONUT_H
#define DONUT_H

#include <stdio.h>
#include <stdlib.h>

typedef enum boolean boolean;
typedef struct CharList CharList;
typedef CharList * CharListPtr;
typedef struct String String;
typedef String * StringPtr;

enum boolean {
    false, true
};

struct CharList
{
    char char_unit;
    CharListPtr next_ptr;
    CharListPtr previous_ptr;
};

struct String
{
    CharListPtr char_list_header;
    CharListPtr char_list_tail;
    unsigned length;
    StringPtr id;
};

const int INDEX_OUT_OF_BOUNDS_ERROR_CODE = 37;

void append_char_unit(CharListPtr *char_list_header_ptr, CharListPtr *char_list_tail_ptr, char char_unit);
unsigned length(String string);
/*
 * Creates a String from a char array
 */
String _String(const char *char_array)
{
    StringPtr string = (StringPtr) malloc(sizeof(String));
    if (string != NULL)
    {
        int index = 0;
        string->char_list_header = NULL;
        string->char_list_tail = NULL;
        while (*(char_array + index) != '\0')
        {
            append_char_unit(&(string->char_list_header),
                             &(string->char_list_tail), *(char_array + index++));
        }
        string->length = index;
        string->id = string;
        return *string;
    }
    else
    {
        _String(char_array);
    }
    return *string;
}


char * convert_to_char_array(String string);
/*
 * Creates a String using another
 */
String _StringWithAnother(String string)
{
    return _String(convert_to_char_array(string));
}

/*
 * Appends a character at the end of a CharList structure
 */
void append_char_unit(CharListPtr *char_list_header_ptr, CharListPtr *char_list_tail_ptr, char char_unit)
{
    CharListPtr new_char_list_ptr = (CharListPtr) malloc(sizeof(CharList));
    if (new_char_list_ptr != NULL)
    {
        new_char_list_ptr->char_unit = char_unit;
        new_char_list_ptr->next_ptr = NULL;
        new_char_list_ptr->previous_ptr = NULL;
        if (*char_list_header_ptr == NULL)
        {
            *char_list_header_ptr = new_char_list_ptr;
            *char_list_tail_ptr = new_char_list_ptr;
            return;
        }
        CharListPtr current_char_list_ptr = *char_list_header_ptr;
        while (current_char_list_ptr->next_ptr != NULL)
        {
            current_char_list_ptr = current_char_list_ptr->next_ptr;
        }
        *char_list_tail_ptr = new_char_list_ptr;
        new_char_list_ptr->previous_ptr = current_char_list_ptr;
        current_char_list_ptr->next_ptr = new_char_list_ptr;
    }
    else
    {
        append_char_unit(char_list_header_ptr, char_list_tail_ptr, char_unit);
    }
}

/*
 * Finds the length of a given String
 */
unsigned length(String string)
{
    CharListPtr current_char_list_ptr = string.char_list_header;
    unsigned length = 0;
    while (current_char_list_ptr != NULL)
    {
        current_char_list_ptr = current_char_list_ptr->next_ptr;
        ++length;
    }
    return length;
}

// For string matching: https://en.wikipedia.org/wiki/String-searching_algorithm
// Plan: Implement one of them. Current one: Naive string matching
char char_at(String string, unsigned index);
/*
 * Finds the key String's position in given String
 * Returns -1 if cannot find it
 */
int index_of(String string, String key)
{
    int index = -1;
    boolean is_key_found = false;
    for (int i = 0; i <= string.length - key.length; ++i)
    {
        for (unsigned j = 0; j < key.length; ++j)
        {
            is_key_found = true;
            if (char_at(string, i + j) != char_at(key, j))
            {
                is_key_found = false;
                break;
            }
        }
        if (is_key_found)
        {
            index = i;
            break;
        }
    }
    return index;
}

/*
 * Returns the character in a String at the given index
 * Gives an error if index is out of bounds
 */
char char_at(String string, unsigned index)
{
    if (index >= string.length)
    {
        fprintf(stderr, "%s\n", "Error: Index out of bounds.");
        exit(INDEX_OUT_OF_BOUNDS_ERROR_CODE);
    }
    if (index < (int)(string.length / 2 + 0.5))
    {
        CharListPtr current_char_list_ptr = string.char_list_header;
        for (unsigned i = 0; i < index; ++i)
        {
            current_char_list_ptr = current_char_list_ptr->next_ptr;
        }
        return current_char_list_ptr->char_unit;
    }
    else
    {
        CharListPtr current_char_list_ptr = string.char_list_tail;
        for (unsigned i = 0; i < string.length - index - 1; ++i)
        {
            current_char_list_ptr = current_char_list_ptr->previous_ptr;
        }
        return current_char_list_ptr->char_unit;
    }
}

/*
 * Reverses a String without creating a new one
 */
void reverse(String string)
{
    CharListPtr left_char_list_ptr = string.char_list_header;
    CharListPtr right_char_list_ptr = string.char_list_tail;
    for (int i = 0; i < string.length / 2; ++i)
    {
        char temp = left_char_list_ptr->char_unit;
        left_char_list_ptr->char_unit = right_char_list_ptr->char_unit;
        right_char_list_ptr->char_unit = temp;
        left_char_list_ptr = left_char_list_ptr->next_ptr;
        right_char_list_ptr = right_char_list_ptr->previous_ptr;
    }
}

/*
 * Prints the given String in default way
 */
void print_string(String string)
{
    CharListPtr char_list_header = string.char_list_header;
    while (char_list_header != NULL)
    {
        printf("%c", char_list_header->char_unit);
        char_list_header = char_list_header->next_ptr;
    }
    puts("");
}

/*
 * Converts the given String to char array
 */
char * convert_to_char_array(String string)
{
    char * char_array = (char *) malloc(string.length + 1);
    CharListPtr current_char_list_ptr = string.char_list_header;
    int index = 0;
    while (current_char_list_ptr != NULL)
    {
        *(char_array + index++) = current_char_list_ptr->char_unit;
        current_char_list_ptr = current_char_list_ptr->next_ptr;
    }
    *(char_array + index) = '\0';
    return char_array;
}

/*
 * Copies the given String then returns the copy
 * Function implements deep copy
 */
String copy(String string)
{
    return _String(convert_to_char_array(string));
}

/*
 * Prints a String in debug format
 */
void __print_string(String string)
{
    int index = 0;
    CharListPtr current_ptr = string.char_list_header;
    printf("ADDR[%d]: %p\n", index++, current_ptr);
    while (current_ptr != NULL)
    {
        printf("CHAR[%d]: %c\n", index - 1, current_ptr->char_unit);
        current_ptr = current_ptr->next_ptr;
        printf("ADDR[%d]: %p\n", index++, current_ptr);
    }
}

/*
 * Inserts a String into another String using the given index
 * If index is less that 0 or greater than first String it gives error
 */
void insert(StringPtr core_ptr, String node, int index)
{
    if (index > (*core_ptr).length || index < 0)
    {
        fprintf(stderr, "%s\n", "Error: Index out of bounds.");
        exit(INDEX_OUT_OF_BOUNDS_ERROR_CODE);
    }
    String node_mimic = copy(node);
    if (index == 0)
    {
        node_mimic.char_list_tail->next_ptr = core_ptr->char_list_header;
        core_ptr->char_list_header = node_mimic.char_list_header;
        (*core_ptr).length = length(*core_ptr);
        return;
    }
    CharListPtr current_core_ptr = (*core_ptr).char_list_header;
    for (int i = 0; i < index - 1; i++)
    {
        current_core_ptr = current_core_ptr->next_ptr;
    }
    CharListPtr linking_core_ptr = current_core_ptr->next_ptr;
    current_core_ptr->next_ptr = node_mimic.char_list_header;
    node_mimic.char_list_tail->next_ptr = linking_core_ptr;
    if (linking_core_ptr != NULL) linking_core_ptr->previous_ptr = node_mimic.char_list_tail;
    (*core_ptr).length = length(*core_ptr);
}

/*
 * Puts the node at the end of the core
 */
void append(StringPtr core_ptr, String node)
{
    insert(core_ptr, node, (int) (core_ptr->length));
}

/*
 * Same as append just for convention
 */
void concat(StringPtr core_ptr, String node)
{
    append(core_ptr, node);
}

/*
 * Puts the node at the beginning of the core
 */
void prepend(StringPtr core_ptr, String node)
{
    insert(core_ptr, node, 0);
}

/*
 * First mapper function
 * Converts a character to uppercase
 */
char __MapToUpperCase(char input)
{
    char output;
    if (input < 'a' || input > 'z') return input;
    output = (char) (input - ('a' - 'A'));
    return output;
}

/*
 * Second mapper function
 * Converts a character to lowercase
 */
char __MapToLowerCase(char input)
{
    char output;
    if (input < 'A' || input > 'Z') return input;
    output = (char) (input + ('a' - 'A'));
    return output;
}

/*
 * Maps the given String using the given mapper
 * Remember mappers start with __(double underscores) for convention
 */
void map(String string, char (__Map)(char))
{
    CharListPtr current_char_list_ptr = string.char_list_header;
    while (current_char_list_ptr != NULL)
    {
        current_char_list_ptr->char_unit = __Map(current_char_list_ptr->char_unit);
        current_char_list_ptr = current_char_list_ptr->next_ptr;
    }
}

/*
 * Shrinks a String using the begin and end index
 * End index is exclusive
 */
void shrink(StringPtr string_ptr, int begin_index, int end_index)
{
    unsigned len = length(*string_ptr);
    if (begin_index > end_index || begin_index > len || begin_index < 0 || end_index > len || end_index < 0)
    {
        fprintf(stderr, "%s\n", "Error: Index out of bounds.");
        exit(INDEX_OUT_OF_BOUNDS_ERROR_CODE);
    }
    CharListPtr forward_ptr = (*string_ptr).char_list_header;
    CharListPtr backup_ptr;
    for (int i = 0; i < begin_index; i++)
    {
        backup_ptr = forward_ptr;
        forward_ptr = forward_ptr->next_ptr;
        free(backup_ptr);
    }
    (*string_ptr).char_list_header = forward_ptr;
    CharListPtr backward_ptr = (*string_ptr).char_list_tail;
    for (int j = 0; j < len - end_index; ++j)
    {
        backup_ptr = backward_ptr;
        backward_ptr = backward_ptr->previous_ptr;
        free(backup_ptr);
    }
    (*string_ptr).char_list_tail = backward_ptr;
}

/*
 * Creates the substring based on given String and indices
 * End index is exclusive
 */
String substring(StringPtr string_ptr, int begin_index, int end_index)
{
    String replica = copy(*string_ptr);
    shrink(&replica, begin_index, end_index);
    return replica;
}

/*
 * Returns true if both String have the same reference (or id)
 */
boolean is_same(String string, String compared)
{
    return string.id == compared.id;
}

/*
 * Returns true if both String has the same content
 */
boolean is_equal(String string, String compared)
{
    if (is_same(string, compared))
        return true;
    if (string.length != compared.length)
        return false;
    for (unsigned i = 0; i < string.length; ++i)
        if (char_at(string, i) != char_at(compared, i))
            return false;
    return true;
}

/*
 * Returns true if string contains compared one
 */
boolean contains(String string, String compared)
{
    return index_of(string, compared) != -1;
}

/*
 * Joins a set of Strings into one String using the delimiter
 */
String join(String clusters[], unsigned length, String delimiter)
{
    String string = _String("");
    for (int i = 0; i < length; ++i)
    {
        append(&string, clusters[i]);
        if (i != length - 1)
            append(&string, delimiter);
    }
    return string;
}

/*
 * Returns true if given string is empty ("")
 */
boolean is_empty(String string)
{
    return string.length == 0;
}

/*
 * Replaces old String with the new one in the given string
 */
void replace(String string, String old, String new)
{
    int start_index = index_of(string, old);
    CharListPtr begin_char_list_ptr = string.char_list_header, current_char_list_ptr, freed_char_list_ptr = NULL;
    for (int i = 0; i < start_index - 1; ++i)
        begin_char_list_ptr = begin_char_list_ptr->next_ptr;
    current_char_list_ptr = begin_char_list_ptr;
    for (int i = 0; i <= old.length; ++i)
    {
        freed_char_list_ptr = current_char_list_ptr;
        current_char_list_ptr = current_char_list_ptr->next_ptr;
        if (i > 0)
            free(freed_char_list_ptr);
    }
    boolean is_allocated;
    for (int i = 0; i < new.length; ++i)
    {
        is_allocated = false;
        while (!is_allocated)
        {
            CharListPtr new_char_list_ptr = malloc(sizeof(CharList));
            if (new_char_list_ptr == NULL) continue;
            new_char_list_ptr->previous_ptr = begin_char_list_ptr;
            begin_char_list_ptr->next_ptr = new_char_list_ptr;
            new_char_list_ptr->next_ptr = NULL;
            new_char_list_ptr->char_unit = char_at(new, i);
            begin_char_list_ptr = new_char_list_ptr;
            is_allocated = true;
        }
    }
    begin_char_list_ptr->next_ptr = current_char_list_ptr;
}

/*
 * Creates a new String from the given String
 * Very similar to copy() yet it only copies references
 * They both shows the same String
 */
String intern(String string)
{
    return *string.id;
}

/*
 * Removes spaces from the beginning and the end of the given string
 */
void trim(StringPtr string_ptr)
{
    CharListPtr freed_char_list_ptr = NULL;
    while (string_ptr->char_list_header->char_unit == ' ')
    {
        freed_char_list_ptr = string_ptr->char_list_header;
        string_ptr->char_list_header = string_ptr->char_list_header->next_ptr;
        free(freed_char_list_ptr);
    }
    while (string_ptr->char_list_tail->char_unit == ' ')
    {
        freed_char_list_ptr = string_ptr->char_list_tail;
        string_ptr->char_list_tail = string_ptr->char_list_tail->previous_ptr;
        free(freed_char_list_ptr);
    }
}

#endif