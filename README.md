# [Donut](https://en.wikipedia.org/wiki/String_(computer_science))  [![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0) [![Generic badge](https://img.shields.io/badge/version-v0.2.1-brightgreen.svg)](https://shields.io/)  [![Maintenance](https://img.shields.io/badge/Maintained%3F-yes-blueviolet.svg)](https://GitHub.com/Naereen/StrapDown.js/graphs/commit-activity)
Donut is a C library created for string manipulation.
* __Mutable:__ The most robust aspect of this library is its mutability. With this feature, changing a string or adding substrings before or after it is very easy. And this method gives the programmer a lot of flexibility.
* __Brand New Declaration:__ Creating a string in C nowadays can be really complicated without any interface. Ctring deals with this problem in a very delicate way.
* __Miscellaneous and Plenty Functions:__ Reversing a string, finding a substring in another string or even converting uniquely defined string structure to a character array. All of them defined in this minimal yet powerful library.
# Documentation
Donut is documented fully in source file.
Also it has a wiki page to be created soon.

You can improve it by sending pull requests to [this repository.](https://gitlab.com/mberakoc/donut)
# Examples
You can start using library just including it like below:
```c
#include "donut.h"
```
In wiki you can find a lot of examples for each future.Here is the first one to get you started:
```c
#include "donut.h"

int main(int argc, char const *argv[])
{
  String string = _String("Hello World!");
  print_string(string);
  return EXIT_SUCCESS;
}
```

This example will print "Hello World!" string on the screen.

You'll notice how easy to create a string only using `_String` function without struggling all these complex middleware. This is one the most powerful sides of this library. It offers a well-thought abstraction. Without taking any control it gives you ligthweight coding benefit.

## Licence 
Ctring is [GNU Licensed.](https://gitlab.com/mberakoc/donut/blob/master/LICENSE)
